# Wireless Serial Gateway 

A DIY device that allows me to access serial ports over a browser using Wi-Fi on an ESP8266 board ( Wemos D1 Mini)


## Wemos D1 Mini 

- Amazon - https://geni.us/wemos1min
- AliExpress - https://s.click.aliexpress.com/e/_Af36zD

** affiliate link

## Videos / How-To


[![MrDIY Wireless Serial Gateway YouTube video](https://img.youtube.com/vi/2gElZGItCJQ/0.jpg)](https://youtu.be/2gElZGItCJQ)

Watch the full video - https://youtu.be/2gElZGItCJQ

## Thanks
Many thanks to all the authors and contributors to the open source libraries and community. You are the reason this project exists.

